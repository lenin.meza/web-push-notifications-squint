# WebPushNotificationsSquint

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.1.1.

And updated with ng-cli:

```bash
ng update @angular/cli @angular/core --all=true --allowDirty=true --force=true
```

Based on [Getting started with service workers](https://angular.io/guide/service-worker-getting-started) i add the service worker dependencies:

```bash
ng add @angular/pwa --project web-push-notifications-squint
```

## Firebase

I follow the next guide [Deploying Angular 6 Applications to Firebase Hosting.](https://medium.com/@longboardcreator/deploying-angular-6-applications-to-firebase-hosting-b5dacde9c772)

```bash
npm install -g firebase-tools
```

Login with an active account:

```bash
firebase login

Waiting for authentication...

✔  Success! Logged in as lenin.meza@teamknowlogy.com
```

Initialize the project:

```bash
firebase init

? Which Firebase CLI features do you want to set up for this folder? Press Space to select features, then Enter to confirm your choices. 

Database: Deploy Firebase Realtime Database Rules, Hosting: Configure and deploy Firebase Hosting sites

? What do you want to use as your public directory? dist/web-push-notifications-squint
? Configure as a single-page app (rewrite all urls to /index.html)? Yes
? File dist/web-push-notifications-squint/index.html already exists. Overwrite? No
```

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
